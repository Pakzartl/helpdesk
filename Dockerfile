FROM php:8-fpm-alpine

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    # install-php-extensions gd xdebug csv gmp imagick imap http ldap mcrypt memcache memcached mongodb mysqli oauth odbc opcache pcntl pdo_mysql pdo_odbc pdo_pgsql pgsql redis soap sockets xmlrpc xsl yaml yar zip
    install-php-extensions pdo_mysql

EXPOSE 9000