<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function tickets(Request $request)
    {
        return view('admin.tickets');
    }

    public function ticket(Request $request)
    {
        return view('admin.ticket');
    }
}
