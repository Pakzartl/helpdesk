<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserTicket;
use App\Models\Ticket;
use Carbon\Carbon;
use Exception;
use DB;

class ApiController extends Controller
{

    public function sendIssue(Request $request)
    {
        try {
            $validate = [
                'title' => 'required',
                'description' => 'required',
                'email' => 'required|email'
            ];

            $messages = [
                'title.required' => 'Please Enter Title',
                'description.required' => 'Please Enter Description',
                'email.required' => 'Please Enter E-mail',
                'email.email' => 'Invalid Email'
            ];

            $validator = Validator::make($request->all(), $validate, $messages);

            if (!empty($validator->errors()->first())) {
                throw new Exception($validator->errors()->first(), 1);
            }

            $now = Carbon::now();

            $params = $request->all();

            $ticket = Ticket::create($params);

            return response()->json([
                "status" => "success",
                "message" => "ส่งข้อมูลเรียบร้อย",
                "data" => $ticket->title
            ], 200);
        } catch (Exception $e) {
            return response()->json(["status" => "error", "message" => $e->getMessage()]);
        }
    }
    
    public function getTickets(Request $request)
    {
        $status = collect($request->all())->filter(function($item, $key) {
            return $item == "true";
        });

        $statusMap = collect($status)->map(function($item, $key) {
            return $key;
        })->flatten()->toArray();

        $tickets = Ticket::whereIn('status', $statusMap)->orderBy('status', 'asc')->orderBy('updated_at', 'desc')->get();

        return $tickets;
    }

    public function getTicket(Request $request)
    {
        $ticket = Ticket::where('id', $request->id)->first();
        return $ticket;
    }

    public function updateStatusTicket(Request $request)
    {
        $params = $request->params;
        $state = strtolower($params['state']);

        $ticket = Ticket::where('id', $params['ticket']['id'])->first();
        $ticket->status = $state;
        $ticket->save();
        return [
            'status' => 200,
            'message' => 'change state success',
            'data' => $ticket
        ];
    }
}
